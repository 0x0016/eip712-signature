import React, { Component } from "react";
import Web3 from "web3";
// import SimpleContract from "./Contract.json";
import getWeb3 from "./getWeb3";
import "./App.css";

const contractAddress = "0x2a980B680Aed6BeC1f02Dbe14ddC28Dc9680D73e";

const web3 = new Web3(Web3.givenProvider || "http://localhost/8545");



class App extends Component {
  state = { storageValue: 0, web3: null, accounts: null, contract: null };
  

  connectMetaMask = async () => {
    let accounts;
    try {
      await web3.givenProvider.request({ method: "eth_requestAccounts" });
      //setMetaMaskObject({ metaMaskConnected: true, metaMaskPresent });
      accounts = await web3.eth.getAccounts();
      const networkId = await web3.eth.net.getId();
      if (networkId !== 4) {
          alert("必須使用 Rinkeby 網絡！");
          await web3.currentProvider.request({
            method: "wallet_switchEthereumChain",
            params: [{ chainId: "0x4" }]
          });
      }
      //setPublicKey(() => accounts[0]);
      this.setState({ web3, accounts }, this.initData);
    } catch (error) {
      console.error("metmask error", error);
    }
  };

  componentDidMount = async () => {
    try {

      this.connectMetaMask();
 
      //const web3 = await getWeb3();
      // const accounts = await web3.eth.getAccounts();
      // const networkId = await web3.eth.net.getId();
      // if (networkId !== 4) {
      //     alert("必須使用 Rinkeby 網絡！");
      //     await web3.currentProvider.request({
      //       method: "wallet_switchEthereumChain",
      //       params: [{ chainId: "0x4" }]
      //     });
      // }

      // this.setState({ web3, accounts }, this.initData);
    } catch (error) {
       alert(
        `Failed to load required libraries.`
      );
      console.error(error);
    }
  };

  

  signEIP712Data = async () => {
    const { web3, accounts, contract } = this.state;
    var signer = accounts[0];
     
    await web3.currentProvider.sendAsync(
      {
        method: "net_version",
        params: [],
        jsonrpc: "2.0",
      },
      function (err, result) {
        const netId = result.result;
        console.log("netId", netId);
        const chainId = netId;

        const eip712Obj = {
          types: {
            EIP712Domain: [
              { name: "name", type: "string" },
              { name: "version", type: "string" },
              { name: "chainId", type: "uint256" },
              { name: "verifyingContract", type: "address" },
            ],
            Test: [
              { name: 'owner', type: 'address' },
              { name: 'amount', type: 'uint256' },
              { name: 'nonce', type: 'uint256' },
            ],
          },

          domain: (chainId: number) => ({
            name: "Frank 0x0016",
            version: "1",
            chainId: chainId,
            verifyingContract: contractAddress,
          }),
        };

        const data = JSON.stringify({
          types: eip712Obj.types,
          domain: eip712Obj.domain(chainId),  
          primaryType: "Test",  
          message: {
            owner: signer,
            amount: 120,
            nonce: 2374,
          },
        });

        console.log("data", data);

        web3.currentProvider
          .request({
            method: "eth_signTypedData_v4",
            params: [signer, data], 
          })
          .then((result: string) => {
            const signature = result.substring(2);
            const r = "0x" + signature.substring(0, 64);
            const s = "0x" + signature.substring(64, 128);
            const v = parseInt(signature.substring(128, 130), 16);
            console.log("Result:", { r, s, v });

            contract.methods
              .testEIP712(signer, 120, v, r, s)
              .send({ from: accounts[0] });
          });

        
      }
    );
  };


  signPersonalSign = async () => {
    const { web3, accounts } = this.state;
    var signer = accounts[0];
     
    web3.currentProvider
          .request({
            method: "personal_sign",
            params: [signer, "Hello, this is some human readable message for you to sign. \n\nThis will not cost you any gas and should not be mark as dangerous by MataMask."], 
          })
          .then((result) => {
             console.log(result);
          });

  };


  signETHSignData = async () => {
    const { web3, accounts } = this.state;
    var signer = accounts[0];
     
    web3.currentProvider
          .request({
            method: "eth_sign",
            params: [signer, web3.utils.sha3('Hello world')], 
          })
          .then((result) => {
             console.log(result);
          });

  };

  
  render() {
    if (!this.state.web3) {
      return <div>
        <button onClick={() => this.connectMetaMask()}> 連接 MetaMask </button>
      </div>;
    }
    return (
      <div className="App">
        <h2>EIP712 簽名</h2>
        <p>
         請確認目前是在 Rinkeby 測試網路。打開這個頁面會彈出 metamask 請求錢包連接。
        </p>

        <center>
        <div style={{width: "80%", textAlign: "left", backgroundColor: "#efefef", padding:"30px", borderRadius: "15px", margin: "20px"}}>
         <h3>personal_sign Sign</h3>
         <p>調用 personal_sign， 在簽名信息前加入 prefix 來避免未授權使用. 可顯示 UTF-8 編碼的文字，讓簽名者明確正在簽名什麼內容。這種方式多用在網站登錄。</p>
         <h3>eth_sign Sign</h3>
         <p>eth_sign 必須傳入一個 32 byte 的 message hash 以供簽名，因此，僅憑 message hash 簽名者不會知道自己正在簽署什麼內容，可能是一個交易資料，又或者其它任何內容，<strong>因此有潛在的被釣魚的風險。 MetaMask 會彈出紅色警告。</strong></p>
         <h3>EIP712 Sign</h3>
         <p> EIP712 Sign 是根據 EIP712 標準為 signature 加入了如 domain，contract address 等資訊，從而提高簽名的安全性。</p>
        </div>
        </center>
        
        
        <button onClick={() => this.signPersonalSign()}> personal_sign Sign </button>
        <button onClick={() => this.signETHSignData()}> eth_sign Sign </button>
        <button onClick={() => this.signEIP712Data()}> EIP712 Sign </button>
      </div>
    );
  }
}

export default App;
